#include <stdio.h>
#include <stdlib.h>
#include <math.h>
const int N = 3;
const double
Pi = 3.14159265358979311600e+00,
epsilon = 1e-5,
little_epsilon = 1e-10;

void matrixVectorMul(const int matrixSize, const double *matrix, const double *vector, double *result){
    for (int i = 0; i < matrixSize; ++i) {
        result[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            result[i] += *(matrix+matrixSize*i+j) * vector[j];
        }
    }
}
void scalarVectorMul(const int vectorSize, const double scalar, const double *vector, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector[i] * scalar;
    }
}
void vectorVectorSub(const int vectorSize, const double *vector1, const double *vector2, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector1[i] - vector2[i];
    }
}
double vectorVectorScalarMul(const int vectorSize, const double *vector1, const double *vector2){
    double result = 0.0; // 0_0
    for (int i = 0; i < vectorSize; ++i) {
        result += vector1[i] * vector2[i];
    }
    return result;
}
double vectorLength(const int vectorSize, const double *vector){
    double result = 0.0;
    for (int i = 0; i < vectorSize; ++i) {
        result += vector[i]*vector[i];
    }
    return sqrt(result);
}
void copyFirstVectorToSecond(const int vectorSize, const double *vector1, double *vector2){
    for (int i = 0; i < vectorSize; ++i) {
        vector2[i] = vector1[i];
    }
}
void matrixPrint(const int matrixSize, const double *matrix){
    for (int i = 0; i < matrixSize; ++i) {
        printf("    ( ");
        for (int j = 0; j < matrixSize; ++j) {
            printf("%3.2f ", *(matrix+matrixSize*i+j));
        }
        printf(")\n");
    }
}
void vectorPrint(const int vectorSize, const double *vector){
    printf("( ");
    for (int i = 0; i < vectorSize; ++i) {
        printf("%3.2f ", *(vector+i));
    }
    printf(")\n");
}
void initFixedDecision(const int matrixSize, double *matrix, double *vectorB, double *vectorX){
    for (int i = 0; i < matrixSize; ++i) {
        *(vectorB+i) = matrixSize + 1;
        *(vectorX+i) = 0;
        for (int j = 0; j < matrixSize; ++j) {
            if (i == j){
                *(matrix+i*matrixSize+j) = 2.0;
            } else{
                *(matrix+i*matrixSize+j) = 1.0;
            }
        }
    }
}
void initProisvolDecision(const int matrixSize, double *matrixA, double *vectorB, double *vectorX){
    double* u = (double*)calloc(matrixSize, sizeof(double));
    for (int i = 0; i < matrixSize; ++i) {
        u[i] = sin(2*Pi/matrixSize);
        *(vectorX+i) = 0;
        for (int j = 0; j < matrixSize; ++j) {
            if (i == j){
                *(matrixA+i*matrixSize+j) = 2.0;
            } else{
                *(matrixA+i*matrixSize+j) = 1.0;
            }
        }
    }
    matrixVectorMul(matrixSize, matrixA, u, vectorB);
    vectorPrint(matrixSize, u);
    free(u);
}
void initManual(const int matrixSize, double *matrixA, double *vectorB, double *vectorX){
    double* u = (double*)calloc(matrixSize, sizeof(double));
    printf("%s", "print matrix:\n");
    for (int i = 0; i < matrixSize; ++i) {
        u[i] = sin(2*Pi/matrixSize);
        *(vectorX+i) = 0;
        for (int j = 0; j < matrixSize; ++j) {
            scanf("%lf", (matrixA+i*matrixSize+j));
        }
    }
    printf("%s", "print vector b:\n");
    for (int i = 0; i < matrixSize; ++i) {
        scanf("%lf", (vectorB+i));
    }
    free(u);
}
void printResult(const int size, const int count, const double *matrixA, const double *vectorB, const double *vectorX){
    printf("%s\n", "--------RESULT--------");
    printf("%s %d\n", "iterations count =", count);
    printf("%s", "x = ");
    vectorPrint(size, vectorX); // result
    printf("%s", "b = ");
    vectorPrint(size, vectorB);
    printf("%s\n", "A = ");
    matrixPrint(size, matrixA);
    printf("%s\n", "------RESULT END------");
}

int main() {
    double* A = (double*)calloc(N*N, sizeof(double));
    double* b = (double*)calloc(N, sizeof(double));
    double* x = (double*)calloc(N, sizeof(double));

    initFixedDecision(N, A, b, x);
    //initProisvolDecision(N, A, b, x, result);
    //initManual(N, A, b, x, result);

    double* Ax = (double*)calloc(N, sizeof(double));
    double* y = (double*)calloc(N, sizeof(double));
    double* Ay = (double*)calloc(N, sizeof(double));
    double* tau_y = (double*)calloc(N, sizeof(double));
    double* x_n_plus_one = (double*)calloc(N, sizeof(double));
    double tempValueForCompare = 1;
    double tau;
    int count = 0;

    while ((tempValueForCompare - epsilon) > 0){
        count++;
        matrixVectorMul(N, A, x, Ax);  // Ax = A * x;
        vectorVectorSub(N, Ax, b, y);  // y = Ax - b;
        matrixVectorMul(N, A, y, Ay);  // Ay = A * y;

        if (((vectorVectorScalarMul(N, Ay, Ay) - little_epsilon) < 0))
            tau = 0.0;
        else
            tau = vectorVectorScalarMul(N, y, Ay) / vectorVectorScalarMul(N, Ay, Ay);

        scalarVectorMul(N, tau, y, tau_y);           // tau_y = tau * y;
        vectorVectorSub(N, x, tau_y, x_n_plus_one);  // x_n_plus_one = x - tau*y

        if (((vectorLength(N, b) - little_epsilon) < 0))
            tempValueForCompare = 0.0;
        else tempValueForCompare = vectorLength(N, y)/vectorLength(N, b);       // ||Ax - b||/||b|| < E

        copyFirstVectorToSecond(N, x_n_plus_one, x);
    }
    printResult(N, count, A, b, x);  // printing result

    free(x);
    free(A);
    free(b);
    free(Ax);
    free(y);
    free(Ay);
    free(tau_y);
    free(x_n_plus_one);
    return 0;
}
